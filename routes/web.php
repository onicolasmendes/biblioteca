<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\LivroController;

Route::get('/', [LivroController::class, 'index'])->name('home');

Route::get('/livros', [LivroController::class, 'indexx'])->name('listagem_livros');

Route::get('/editoras', [LivroController::class, 'lieditoras'])->name('listagem_editoras');

Route::get('/autores', [LivroController::class, 'liautores'])->name('listagem_autores');

Route::get('/editlivros', [LivroController::class, 'edlivros'])->name('editar_livros');

Route::get('/editeditoras', [LivroController::class, 'ededitoras'])->name('editar_editoras');

Route::get('/editautores', [LivroController::class, 'edautores'])->name('editar_autores');

Route::match(['get', 'post'], '/editautores/{id}', ['as' => 'editar_autor', LivroController::class, 'edit_autor'])->name('editar_autor');

Route::post('/salva_autor', [LivroController::class, 'salva_autor'])->name('salva_autor');

Route::match(['get', 'post'], '/editeditoras/{id}', ['as' => 'editar_editora', LivroController::class, 'edit_editora'])->name('editar_editora');

Route::post('/salva_editora', [LivroController::class, 'salva_editora'])->name('salva_editora');

Route::match(['get', 'post'], '/editlivros/{id}', ['as' => 'editar_livro', LivroController::class, 'edit_livro'])->name('editar_livro');

Route::post('/salva_livro', [LivroController::class, 'salva_livro'])->name('salva_livro');

Route::match(['get', 'post'], '/inserirautor', ['as' => 'inserir_autor', LivroController::class, 'inserir_autor'])->name('inserir_autor');

Route::post('/salva_novo_autor', [LivroController::class, 'salva_novo_autor'])->name('salva_novo_autor');

Route::match(['get', 'post'], '/inserireditora', ['as' => 'inserir_editora', LivroController::class, 'inserir_editora'])->name('inserir_editora');

Route::post('/salva_nova_editora', [LivroController::class, 'salva_nova_editora'])->name('salva_nova_editora');

Route::match(['get', 'post'], '/inserirlivro', ['as' => 'inserir_livro', LivroController::class, 'inserir_livro'])->name('inserir_livro');

Route::post('/salva_novo_livro', [LivroController::class, 'salva_novo_livro'])->name('salva_novo_livro');

Route::match(['get', 'post'], '/deletar_editora/{id}', ['as' => 'deletar_editora', LivroController::class, 'deletar_editora'])->name('deletar_editora');

Route::match(['get', 'post'], '/deletar_autor/{id}', ['as' => 'deletar_autor', LivroController::class, 'deletar_autor'])->name('deletar_autor');

Route::match(['get', 'post'], '/deletar_livro/{id}', ['as' => 'deletar_livro', LivroController::class, 'deletar_livro'])->name('deletar_livro');