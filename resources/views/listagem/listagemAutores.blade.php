@extends('template.template_base')

@section('titulo')
Autores
@endsection

@section('conteudo')
   
<div id="dados">
        <h3 class="titulo">Autores</h3>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID do autor</th>
                    <th scope="col">Nome</th>
                </tr>
            </thead>
            <tbody>
                @foreach($autores as $autores)
                <tr>

                    <td>{{$autores->id}}</td>
                    <td>{{$autores->nome}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
@endsection