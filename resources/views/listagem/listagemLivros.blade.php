@extends('template.template_base')

@section('titulo')
Livros
@endsection

@section('conteudo')
 
<div id="dados">
        <h3 class="titulo">Livros</h3>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID do livro</th>
                    <th scope="col">Título do livro</th>
                    <th scope="col">ID do autor</th>
                    <th scope="col">ID da editora</th>
                    <th scope="col">Local do livro</th>
                </tr>
            </thead>
            <tbody>
                @foreach($livros as $livros)
                <tr>

                    <td>{{$livros->id}}</td>
                    <td>{{$livros->titulo}}</td>
                    <td>{{$livros->id_autor}}</td>
                    <td>{{$livros->id_editora}}</td>
                    <td>{{$livros->local}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
@endsection

