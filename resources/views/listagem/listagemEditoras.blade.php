@extends('template.template_base')

@section('titulo')
Editoras
@endsection

@section('conteudo')
    
<div id="dados">
        <h3 class="titulo">Editoras</h3>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID da editora</th>
                    <th scope="col">Nome</th>
                </tr>
            </thead>
            <tbody>
                @foreach($editoras as $editoras)
                <tr>
                    <td>{{$editoras->id}}</td>
                    <td>{{$editoras->nome}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        
    </div>
    
@endsection