<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('titulo')</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/e827c10325.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <!-- Styles -->
    <style>
        img {
            margin-left: auto;
            margin-right: auto;
            display: block;
            margin-top: 4%;
            width: 22%;
            height: 22%;
        }

        #btn_new {
            display: block;
            margin-left: 80%;
        }

        h1 {
            font-family: 'Poppins';
        }

        button {
            font-family: 'Poppins';
        }

        .nome_titulo {
            text-align: center;
            font-weight: 900;
        }

        .inputtxt {
            width: 40%;
            margin-left: auto;
            margin-right: auto;

        }

        .label_nome {
            text-align: center;
            top: 30%;
            font-weight: bold;
        }

        .btnsubmit {
            margin-left: auto;
            margin-right: auto;
            display: block;
        }

        body {
            font-family: 'Nunito', sans-serif;
            background-color: #ededdf;
        }

        h1 {
            color: #4266eb;
            font-weight: 900;
            text-align: center;
            font-size: 50px;
            color: #4266eb;
            font-weight: 900;
            text-align: center;

        }

        button {
            height: auto;
            font-weight: 900;
            font-family: 'Nunito', sans-serif;
            font-size: 30px;
            border-radius: 10px;
            display: block;
            margin-left: auto;
            margin-right: auto;
            margin-top: 20px;
            background-color: #B55EF7;
        }

        button:hover {
            cursor: pointer;
        }

        #dados {
            background-color: #EDC05A;
            display: block;
            margin-left: auto;
            margin-right: auto;
            margin-top: 5px;
            width: 1200px;
            height: auto;
            border-radius: 30px;
            font-family: 'Poppins';
            padding: 12px;
        }

        .titulo {
            font-size: 50px;
            text-align: center;
            font-weight: 900;
        }

        footer {
            text-align: center;
            margin-top: 5%;
            font-weight: 700;
        }

        a:link {
            text-decoration: none;
        }

        #citacao {
            font-family: 'Poppins';
            font-weight: bold;
            text-align: center;
            font-size: 150%;
        }

        #select_autor {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

    </style>
</head>

<body class="antialiased">
    <script>
        @yield('script')

    </script>
    <h1>Biblioteca - Nicolas Mendes 📖</h1>

    <!--<nav class="nav nav-pills flex-column flex-sm-row">
        <a class="flex-sm-fill text-sm-center nav-link " href="{{route('home')}}">Página Inicial</a>
        <a class="flex-sm-fill text-sm-center nav-link" href="{{route('listagem_livros')}}">Listagem dos livros</a>
        <a class="flex-sm-fill text-sm-center nav-link " href="{{route('listagem_editoras')}}">Listagem das editoras</a>
        <a class="flex-sm-fill text-sm-center nav-link " href="{{route('listagem_autores')}}">Listagem dos autores</a>
        <a class="flex-sm-fill text-sm-center nav-link" href="{{route('editar_livros')}}">Editar livros</a>
        <a class="flex-sm-fill text-sm-center nav-link" href="{{route('editar_editoras')}}">Editar editoras</a>
        <a class="flex-sm-fill text-sm-center nav-link" href="{{route('editar_autores')}}">Editar autores</a>
    </nav>
    </ul>-->
    <ul class="nav nav-pills">
        <li class="nav-item">
            <a class="nav-link" href="{{route('home')}}">Página inicial</a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                aria-expanded="false">Listagem</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="{{route('listagem_livros')}}">Livros</a>
                <a class="dropdown-item" href="{{route('listagem_editoras')}}">Editoras</a>
                <a class="dropdown-item" href="{{route('listagem_autores')}}">Autores</a>

            </div>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                aria-expanded="false">Editar</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="{{route('editar_livros')}}">Livros</a>
                <a class="dropdown-item" href="{{route('editar_editoras')}}">Editoras</a>
                <a class="dropdown-item" href="{{route('editar_autores')}}">Autores</a>

            </div>
        </li>


    </ul>

    <br>

    @yield('conteudo')
    <footer>
        <p>Site desenvolvido por Nicolas Mendes</p>
    </footer>
</body>

</html>
