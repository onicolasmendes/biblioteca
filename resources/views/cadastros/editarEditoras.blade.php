@extends('template.template_base')
@section('titulo')
Editar Editoras
@endsection

@section('script')

var btneditar = document.getElementsByClassName("btneditar");

function editar(clicked_id) {
    var id = clicked_id;
    var url = "{{url('/')}}" + '/editeditoras/' + id;
    //alert(url);
    window.location.href = url;
    //alert("teste");
}

function inserir(){
    var url = "{{url('/')}}" + '/inserireditora';
    //alert(url);
    window.location.href = url;
}

function deletar(clicked_id){
var id = clicked_id;
var resposta = confirm("Deseja excluir a editora ID = "+id+" ?");
if(resposta){
    var url = "{{url('/')}}"+'/deletar_editora/'+id;
    window.location.href = url;
}
}
@endsection

@section('conteudo')
    
    @if ($message = Session::get('error'))
        <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert">X</button>
        {!! $message !!}
        </div>
    @endif
    
    
    <button type="button" class="btn btn-outline-success" id="btn_new" onclick="inserir()"> <span class="fas fa-plus"></span> Inserir uma
        nova editora</button>

    <div id="dados">
        <h3 class="titulo">Editoras</h3>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID da editora</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach($editoras as $editoras)
                <tr>
                    <td>{{$editoras->id}}</td>
                    <td>{{$editoras->nome}}</td>
                    <td>
                        <button id="{{$editoras->id}}" type="button" class="btn btn-secondary btneditar" onclick="editar(this.id)"> <span class="fas fa-edit"> </span> Editar</button> <br>
                        <button id="{{$editoras->id}}" type="button" class="btn btn-danger btndeletar" onclick="deletar(this.id)"> <span class="fas fa-trash-alt"></span>
                            Apagar</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
@endsection