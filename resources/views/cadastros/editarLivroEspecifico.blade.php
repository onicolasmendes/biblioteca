@extends('template.template_base')

@section('titulo')
Editar Livro
@endsection

@section('conteudo')
<div class="row">
    <div class="col-md titulo">
        <h2 class="nome_titulo">Edição do livro: {{$livro->titulo}}</h2>
    </div>
</div>

<form action="{{url('salva_livro')}}" method="post">
    @csrf
    <input type="hidden" name="id" value="{{$livro->id}}">
    <div class="form-group">
        <h4 class="label_nome">Nome do livro:</h4>
        <input type="text" class="form-control inputtxt" id="formGroupExampleInput" name="titulo"
            value="{{$livro->titulo}}">
        <br>
       
       <h4 class="label_nome">Nome do autor:</h4>
        <select name="id_autor" id="select_autor" class="form_control">
            @foreach($autores as $autor)
            @if ($livro->id_autor == $autor->id)
            <option value="{{$autor->id}}" selected>{{$autor->nome}}</option>
            @else
            <option value="{{$autor->id}}">{{$autor->nome}}</option>
            @endif
            @endforeach
        </select>
<br>

        <h4 class="label_nome">Nome da editora:</h4>
        <select name="id_editora" id="select_autor" class="form_control">
            @foreach($editoras as $editora)
            @if ($livro->id_editora == $editora->id)
            <option value="{{$editora->id}}" selected>{{$editora->nome}}</option>
            @else
            <option value="{{$editora->id}}">{{$editora->nome}}</option>
            @endif
            @endforeach
        </select>
        
<br>

        <h4 class="label_nome">Local do livro:</h4>
        <input type="text" class="form-control inputtxt" id="formGroupExampleInput" name="local"
            value="{{$livro->local}}">

    </div>
    <button type="submit" class="btn btn-success btnsubmit">Salvar alterações</button>
</form>

</div>
@endsection
