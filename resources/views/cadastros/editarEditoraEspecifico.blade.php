@extends('template.template_base')

@section('titulo')
Editar Editora
@endsection


@section('conteudo')
    <div class="row">
        <div class="col-md titulo">
            <h2 class="nome_titulo">Edição da editora: {{$editora->nome}}</h2>
        </div>
    </div>

    <form action="{{url('salva_editora')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$editora->id}}">
        <div class="form-group">
            <h4 class="label_nome">Nome da editora:</h4>
            <input type="text" class="form-control inputtxt" id="formGroupExampleInput" name="nome"
                value="{{$editora->nome}}">
        </div>
        <button type="submit" class="btn btn-success btnsubmit">Salvar alterações</button>
    </form>

    </div>
@endsection
