@extends('template.template_base')

@section('titulo')
Editar Livros
@endsection

@section('script')
var btneditar = document.getElementsByClassName("btneditar");

function editar(clicked_id) {
    var id = clicked_id;
    var url = "{{url('/')}}" + '/editlivros/' + id;
    //alert(url);
    window.location.href = url;
    //alert("teste");
}

function inserir(){
    var url = "{{url('/')}}" + '/inserirlivro';
    //alert(url);
    window.location.href = url;
}

function deletar(clicked_id){
var id = clicked_id;
var resposta = confirm("Deseja excluir o livro ID = "+id+" ?");
if(resposta){
    var url = "{{url('/')}}"+'/deletar_livro/'+id;
    window.location.href = url;
}
}

@endsection

@section('conteudo')
   
    @if ($message = Session::get('error'))
        <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert">X</button>
        {!! $message !!}
        </div>
    @endif
    
   
   
    <button type="button" class="btn btn-outline-success" id="btn_new" onclick="inserir()"> <span class="fas fa-plus"></span> Inserir um
        novo livro</button>

    <div id="dados">
        <h3 class="titulo">Livros</h3>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID do livro</th>
                    <th scope="col">Título do livro</th>
                    <th scope="col">ID do autor</th>
                    <th scope="col">ID da editora</th>
                    <th scope="col">Local do livro</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach($livros as $livros)
                <tr>

                    <td>{{$livros->id}}</td>
                    <td>{{$livros->titulo}}</td>
                    <td>{{$livros->id_autor}}</td>
                    <td>{{$livros->id_editora}}</td>
                    <td>{{$livros->local}}</td>
                    <td>
                        <button id="{{$livros->id}}" type="button" class="btn btn-secondary btneditar" onclick="editar(this.id)"> <span class="fas fa-edit"></span> Editar</button> <br>
                        <button id="{{$livros->id}}" type="button" class="btn btn-danger" onclick="deletar(this.id)"> <span class="fas fa-trash-alt"></span>
                            Apagar</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
@endsection