@extends('template.template_base')

@section('titulo')
Editar Autor
@endsection

@section('script')
var btneditar = document.getElementsByClassName("btneditar");

function editar(clicked_id) {
    var id = clicked_id;
    var url = "{{url('/')}}" + '/editautores/' + id;
    //alert(url);
    window.location.href = url;
    //alert("teste");
}
@endsection

@section('conteudo')
    <div class="row">
        <div class="col-md titulo">
            <h2 class="nome_titulo">Edição de: {{$autor->nome}}</h2>
        </div>
    </div>

    <form action="{{url('salva_autor')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$autor->id}}">
        <div class="form-group">
            <h4 class="label_nome">Nome do autor:</h4>
            <input type="text" class="form-control inputtxt" id="formGroupExampleInput" name="nome" value="{{$autor->nome}}">
        </div>
        <button type="submit" class="btn btn-success btnsubmit">Salvar alterações</button>
    </form>

    </div>
@endsection