@extends('template.template_base')

@section('titulo')
Inserir Livro
@endsection

@section('conteudo')
<div class="row">
    <div class="col-md titulo">
        <h2 class="nome_titulo">Inserir livro</h2>
    </div>
</div>

<form action="{{url('salva_novo_livro')}}" method="post">
    @csrf
 
    <div class="form-group">
        <h4 class="label_nome">Nome do livro:</h4>
        <input type="text" class="form-control inputtxt" id="formGroupExampleInput" name="titulo"
           >
        <br>
       
       <h4 class="label_nome">Nome do autor:</h4>
        <select name="id_autor" id="select_autor" class="form_control">
            @foreach($autores as $autor)
          
            <option value="{{$autor->id}}" >{{$autor->nome}}</option>
            
            
            @endforeach
        </select>
<br>

        <h4 class="label_nome">Nome da editora:</h4>
        <select name="id_editora" id="select_autor" class="form_control">
            @foreach($editoras as $editora)
          
            <option value="{{$editora->id}}">{{$editora->nome}}</option>
            
            @endforeach
        </select>
        
<br>

        <h4 class="label_nome">Local do livro:</h4>
        <input type="text" class="form-control inputtxt" id="formGroupExampleInput" placeholder="Ex:S5E2P1" name="local"
           >

    </div>
    <button type="submit" class="btn btn-success btnsubmit">Salvar alterações</button>
</form>

</div>
@endsection
