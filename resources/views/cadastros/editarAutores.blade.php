@extends('template.template_base')

@section('titulo')
Editar Autores
@endsection

@section('script')
var btneditar = document.getElementsByClassName("btneditar");

function editar(clicked_id) {
    var id = clicked_id;
    var url = "{{url('/')}}" + '/editautores/' + id;
    //alert(url);
    window.location.href = url;
    //alert("teste");
}

function inserir(){
    var url = "{{url('/')}}" + '/inserirautor';
    //alert(url);
    window.location.href = url;
}

function deletar(clicked_id){
var id = clicked_id;
var resposta = confirm("Deseja excluir o autor ID = "+id+" ?");
if(resposta){
    var url = "{{url('/')}}"+'/deletar_autor/'+id;
    window.location.href = url;
}
}
@endsection

@section('conteudo')
   
   
   @if ($message = Session::get('error'))
        <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert">X</button>
        {!! $message !!}
        </div>
   @endif
   
   
    <button type="button"  class="btn btn-outline-success" id="btn_new" onclick="inserir()"> <span class="fas fa-plus"></span> Inserir um
        novo autor</button>

    <div id="dados">
        <h3 class="titulo">Autores</h3>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID do autor</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach($autores as $autores)
                <tr>

                    <td>{{$autores->id}}</td>
                    <td>{{$autores->nome}}</td>
                    <td>
                        <button id="{{$autores->id}}" type="button" class="btn btn-secondary btneditar" onclick="editar(this.id)"> <span class="fas fa-edit"></span> Editar</button> <br>
                        <button id="{{$autores->id}}" type="button" class="btn btn-danger btndeletar" onclick="deletar(this.id)"> <span class="fas fa-trash-alt"></span>
                            Apagar</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
@endsection