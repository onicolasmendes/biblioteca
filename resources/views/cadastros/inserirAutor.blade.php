@extends('template.template_base')

@section('titulo')
Inserir autor
@endsection

@section('script')
var btneditar = document.getElementsByClassName("btneditar");

function editar(clicked_id) {
    var id = clicked_id;
    var url = "{{url('/')}}" + '/editautores/' + id;
    //alert(url);
    window.location.href = url;
    //alert("teste");
}
@endsection

@section('conteudo')
    <div class="row">
        <div class="col-md titulo">
            <h2 class="nome_titulo">Inserir novo autor</h2>
        </div>
    </div>

    <form action="{{url('salva_novo_autor')}}" method="post">
        @csrf
        <div class="form-group">
            <h4 class="label_nome">Nome do autor:</h4>
            <input type="text" class="form-control inputtxt" id="formGroupExampleInput" name="nome">
        </div>
        <button type="submit" class="btn btn-success btnsubmit">Salvar alterações</button>
    </form>

    </div>
@endsection