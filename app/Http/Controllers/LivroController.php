<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Autores;

use App\Models\Livros;

use App\Models\Editoras;

class LivroController extends Controller
{
    public function index(){
        
        $livro=Livros::all();
        
        return view('welcome',['livros'=> $livro]);
    }

    public function indexx(){
        
        $livro=Livros::all();
        
        return view('listagem/listagemLivros',['livros'=> $livro]);
    }

    public function lieditoras(){

        $editora=Editoras::all();

        return view('listagem/listagemEditoras', ['editoras'=> $editora]);
    }

    public function liautores(){

        $autor=Autores::all();

        return view('listagem/listagemAutores', ['autores'=>$autor]);
    }

    public function edlivros(){

        $livro=Livros::all();

        return view('cadastros/editarLivros', ['livros'=>$livro]);
    }

    public function edautores(){

        $autor=Autores::all();

        return view('cadastros/editarAutores', ['autores'=>$autor]);
    }

    public function ededitoras(){

        $editora=Editoras::all();

        return view('cadastros/editarEditoras', ['editoras'=>$editora]);
    }


    public function edit_autor($id){
        $autor=Autores::find($id);
        if($autor){
            return view("cadastros/editarAutorEspecifico", ['autor'=>$autor]);
        }else {
            alert('Erro ao acessar lançamento!');
            return redirect()->back()->withInput();
        }
        
        
       // $idedit=$id;
       // return view("cadastros/editarAutorEspecifico",  ['id'=>$idedit, 'autores'=>$autor]);
    }

    public function salva_autor(Request $request){

        //var_dump($request->all());
        //exit;
        $autor=Autores::find($request->id);
        $autor->nome = $request->nome;
        $autor->save();
        return redirect('/editautores');
    }

    
    public function edit_editora($id){
        $editora=Editoras::find($id);
        if($editora){
            return view("cadastros/editarEditoraEspecifico", ['editora'=>$editora]);
        }else {
            alert('Erro ao acessar lançamento!');
            return redirect()->back()->withInput();
        }
    }

    public function salva_editora(Request $request){
        //var_dump($request->all());
        //exit;
        $editora=Editoras::find($request->id);
        $editora->nome = $request->nome;
        $editora->save();
        return redirect('/editeditoras');

    }

    public function edit_livro($id){
        $autores=Autores::all();
        $editoras=Editoras::all();
        $livro=Livros::find($id);
        if($livro){
            return view("cadastros/editarLivroEspecifico", ['livro'=>$livro, 'autores'=>$autores, 'editoras'=>$editoras]);
        }else {
            alert('Erro ao acessar lançamento!');
            return redirect()->back()->withInput();
        }
    }

    public function salva_livro(Request $request){
        //dd($request->all());
        $livro=Livros::find($request->id);
        $livro->titulo = $request->titulo;
        $livro->id_autor = $request->id_autor;
        $livro->id_editora = $request->id_editora;
        $livro->local = $request->local;
        $livro->save();
        return redirect('/editlivros');

    }

    public function inserir_autor(){
        return view("cadastros/inserirAutor");
    }

    public function salva_novo_autor(Request $request){
        //dd($request->all());
        //exit;
        $autor=new Autores;
        $autor->nome = $request->nome;
        $autor->save();
        return redirect('/editautores');

    }

    public function inserir_editora(){
        return view("cadastros/inserirEditora");
    }

    public function salva_nova_editora(Request $request){
        //dd($request->all());
        //exit;
        $editora=new Editoras();
        $editora->nome = $request->nome;
        $editora->save();
        return redirect('/editeditoras');

    }

    public function inserir_livro(){
        $autores=Autores::all();
        $editoras=Editoras::all();
        $livros = Livros::all();
        return view("cadastros/inserirLivro", ['autores'=>$autores, 'editoras'=>$editoras, 'livro'=>$livros]);
    }

    public function salva_novo_livro(Request $request){
        //dd($request->all());
        //exit;
        $livro=new Livros();
        $livro->titulo = $request->titulo;
        $livro->id_autor = $request->id_autor;
        $livro->id_editora = $request->id_editora;
        $livro->local = $request->local;
        $livro->save();
        return redirect('/editlivros');

    }

    public function deletar_editora($id){
        $livro= Livros::where('id_editora', $id)->first();
        if($livro){
            return redirect('/editeditoras')->with('error', "Há livro (s) cadastrado (s) para a editora ID= ".$id.". A editora não pode ser deletada");
        }
        else{
            $editora = Editoras::find($id);
            $editora->delete();
            return redirect('/editeditoras'); 
        }
      }


    public function deletar_autor($id){
      $livro= Livros::where('id_autor', $id)->first();
      if($livro){
          return redirect('/editautores')->with('error', "Há livro (s) cadastrado (s) para o autor (a) ID= ".$id.". O autor (a) não pode ser deletado");
      }
      else{
          $autor = Autores::find($id);
          $autor->delete();
          return redirect('/editautores'); 
      }
    }

    public function deletar_livro($id){
        $livro= Livros::find($id);
        if($livro){
           $livro->delete();
           return redirect('/editlivros');
        }
        else{
            return redirect('/editlivros')->with('error', "Não foi possível deletar o livro selecionado");
      }
}
}